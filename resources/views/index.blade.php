@extends('layout')

@section('content')
<!-- Page content -->

<div class="section-content text-center" style=" background-image: url('images/0.jpg');" id="home">

  <div class="row-sm-12 my-2">
    <div class="col-sm-12">
      <h4 class="text-light"><span>MAY</span></h4>
      <div class="row m-1 align-centre"> 
        <?php
        foreach ($variable as $events) {            
        ?>
        <div class="card box-shadow" style="background-color: rgb(255,255,255,0.5);" > <!-- mx-1 -->
          <div class="card-body">
            <?php echo $events->EventID; ?><br>
            <b> <?php echo $events->Day; ?> <br>
            €35</b><br>
            <button type="button" class="btn btn-primary btn-xs" onclick="document.getElementById('ticketModal').style.display='block'">
              <h>Book Now</h>
            </button>
          </div>
        </div>
        <?php
        }
        ?>
      </div>
    </div>
    <!-- ---------------------------------------------------------------------  -->
    <div class="col-sm-12">
      <h4 class="text-light"><span>JUNE</span></h4>
      <div class="row m-1 align-centre"> 
        <?php
        foreach ($variable as $events) {            
        ?>
        <div class="card box-shadow" style="background-color: rgb(255,255,255,0.5);" > 
          <div class="card-body">
            <?php echo $events->EventID; ?><br>
            <b> <?php echo $events->Day; ?> <br>
            €35</b><br>
            <button type="button" class="btn btn-primary btn-xs" onclick="document.getElementById('ticketModal').style.display='block'">
              <h>Book Now</h>
            </button>
          </div>
        </div>
        <?php
        }
        ?>
      </div>
    </div>
    <!-- ---------------------------------------------------------------------  -->
    <div class="col-sm-12">
      <h4 class="text-light"><span>JULY</span></h4>
      <div class="row m-1 align-centre"> 
        <?php
        foreach ($variable as $events) {            
        ?>
        <div class="card box-shadow" style="background-color: rgb(255,255,255,0.5);" > 
          <div class="card-body">
            <?php echo $events->EventID; ?><br>
            <b> <?php echo $events->Day; ?> <br>
            €35</b><br>
            <button type="button" class="btn btn-primary btn-xs" onclick="document.getElementById('ticketModal').style.display='block'">
              <h>Book Now</h>
            </button>
          </div>
        </div>
        <?php
        }
        ?>
      </div>
    </div>
    <!-- ---------------------------------------------------------------------  -->
    <div class="col-sm-12">
      <h4 class="text-light"><span>AUGUST</span></h4>
      <div class="row m-1 align-centre"> 
        <?php
        foreach ($variable as $events) {            
        ?>
        <div class="card box-shadow" style="background-color: rgb(255,255,255,0.5);" > 
          <div class="card-body">
            <?php echo $events->EventID; ?><br>
            <b> <?php echo $events->Day; ?> <br>
            €35</b><br>
            <button type="button" class="btn btn-primary btn-xs" onclick="document.getElementById('ticketModal').style.display='block'">
              <h>Book Now</h>
            </button>
          </div>
        </div>
        <?php
        }
        ?>
      </div>
    </div>
    <!-- ---------------------------------------------------------------------  -->
    <div class="col-sm-12">
      <h4 class="text-light"><span>SEPTEMBER</span></h4>
      <div class="row m-1 align-centre"> 
        <?php
        foreach ($variable as $events) {            
        ?>
        <div class="card box-shadow" style="background-color: rgb(255,255,255,0.5);" > 
          <div class="card-body">
            <?php echo $events->EventID; ?><br>
            <b> <?php echo $events->Day; ?> <br>
            €35</b><br>
            <button type="button" class="btn btn-primary btn-xs" onclick="document.getElementById('ticketModal').style.display='block'">
              <h>Book Now</h>
            </button>
          </div>
        </div>
        <?php
        }
        ?>
      </div>
    </div>
  </div>

  <!-- <div class="row">
    <table class="table text-center">
        <thead class="thead-light" style="background-color: rgb(255,255,255,0.2);">
          <tr>
            <th colspan="9" align="center"><b>MAY</b></th>  
          </tr>
        </thead>
        <tbody>
          <tr class="">
          <?php
            foreach ($variable as $events) {            
          ?>
            <td style="background-color: rgb(255,255,255,0.5);">
              <div class="card-deck mb-3s text-center">
                  <?php echo $events->EventID; ?><br>
                  <b> <?php echo $events->Day; ?> <br>
                  €35</b><br>
                <button type="button" class="btn btn-primary btn-xs" onclick="document.getElementById('ticketModal').style.display='block'">
                  <h>Book Now</h>
                </button>
              </div>
            </td>
          <?php
            }
          ?>          
          </tr>
        </tbody>
      </table>
  </div> -->   
</div>

<div class="container-fluid text-center" style="max-width:1200px" id="event"> <!-- tab-pane container active -->
  <h2 class="">Zante Event Company</h2>
  <p class=""><i>We love to party</i></p>
  <p class="">Welcome to the Zante Event Company, the home of Zantes best events and things to do while on holiday on this beautiful island.
  Here, we only sell the TOP RATED events and excursions, nothing new or experimental, and nothing that hasn’t already had REAL amazing feedback.
  This ensures that you’ll have the best time possible, and not waste any of your time or money.</p>
    <div class="card-deck mb-3 text-center" style="margin:0 -16px">
      <div class="card mb-3 box-shadow">
        <img src="images/boat.jpg" alt="The Zante Weekender" style="width:100%" class="hover-opacity">
        <div class="card-body">
          <p><b>The Zante Weekender</b></p>
          <p class="opacity">Fri 27 Nov 2020</p>
          <p>Some description of the package goes here.</p>
          <button class="btn btn-md btn-block btn-outline-primary" onclick="document.getElementById('eventDateModal').style.display='block'">Buy Tickets</button>
        </div>
      </div>
      <div class="card mb-3 box-shadow">
        <img src="images/ultimate.jpg" alt="The A-List" style="width:100%" class="hover-opacity">
        <div class="card-body">
          <p><b>The A-List</b></p>
          <p class="opacity">Sat 28 Nov 2020</p>
          <p>Some description of the package goes here.</p>
          <button class="btn btn-md btn-block btn-outline-primary" onclick="document.getElementById('ticketModal').style.display='block'">Buy Tickets</button>
        </div>
      </div>
      <div class="card mb-3 box-shadow">
        <img src="images/white.jpg" alt="The PartyO Package" style="width:100%" class="hover-opacity">
        <div class="card-body">
          <p><b>The PartyO Package</b></p>
          <p class="opacity">Sun 29 Nov 2020</p>
          <p>Some description of the package goes here.</p>
          <button class="btn btn-md btn-block btn-outline-primary" onclick="document.getElementById('eventDateModal').style.display='block'">Buy Tickets</button>
        </div>
      </div>
    </div>
</div>

 <!-- The Contact Section -->
  <div class="container" style="max-width:1200px" id="contact">
    <div>
      <h1 class="text-center"><b>CONTACT</b></h1>
      <p class="text-center"><i>Query? Drop a note!</i></p>
    </div>
    <div class="clearfix"></div>
    <hr>
    <div class="row">
      <div class="col-md-6 text-center">
        <i class="fa fa-map-marker" style="width:30px"></i> Zakinthos, Greece<br>
        <i class="fa fa-phone" style="width:30px"></i> Phone: +30 151515<br>
        <i class="fa fa-envelope" style="width:30px"> </i> Email: example@gmail.com<br>
      </div>
      <div class="col-md-6 text-right">
        <form class="form-horizontal" action="/action_page.php" target="_blank">
          <div class="row" style="margin:0 -16px 8px -16px">
            <div class="col-sm-6">
              <input class="form-control p-2 px-md-4 mb-3" type="text" placeholder="Name" required name="Name">
            </div>
            <div class="col-sm-6">
              <input class="form-control p-2 px-md-4 mb-3" type="text" placeholder="Email" required name="Email">
            </div>
            <div class="col-sm-12">
              <input class="form-control p-2 px-md-4 mb-3" type="text" placeholder="Message" required name="Message">
            </div>
          </div>
          <button class="btn btn-primary p-2 px-md-4 mb-3" type="submit">SEND</button>
        </form>
      </div>
    </div>
  </div>

<!-- location/map -->
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d42458.46876118127!2d20.85084952325827!3d37.72567301742263!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1367392854fcc1eb%3A0xef811ab33ecced70!2sRepublic%20Beach%20Club%20Zante!5e0!3m2!1sen!2snp!4v1581244852730!5m2!1sen!2snp" 
width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

@endsection