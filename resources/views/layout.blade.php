<!DOCTYPE html>
<html>
<head>
	<title>Boat Party</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<link rel="icon" href="images/zante.png">

	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="css/bootstrap.min.css">

	<!-- Custom styles for this template -->
	<link rel="stylesheet" href="css/custom.css">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

	<!-- css/font-awesome.min.css -->
	<style>
		body {font-family: "Lato", sans-serif}
		.mySlides {display: none}
	</style>

</head>
<body>
	<!-- Navbar -->
	<!-- <div class="d-flex flex-row flex-md-column align-items-center p-1 px-md-4 mb-3 bg-white border-bottom box-shadow">
		<h5 class="my-0 mr-md-auto font-weight-normal">ZANTE BOAT PARTY</h5>
		<a class="btn btn-outline-primary" href="#">Sign up</a>
    </div> -->
	<nav class="navbar navbar-expand-md nav-pills bg-white navbar-white sticky-top border-bottom box-shadow">
    <a class="navbar-brand" href="#home" ><img src="images/zante.png" alt="Logo" style="width:5%"></a>
		<button class="navbar-toggler bg-dark navbar-dark" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="collapsibleNavbar">
		  <ul class="navbar-nav">
		    <li class="nav-item">
		      <a class="nav-link text-dark" href="#home">Home</a> <!-- data-toggle="pill" -->
		    </li>
		    <li class="nav-item">
		      <a class="nav-link text-dark" href="#event">Events</a>
		    </li>
		    <li class="nav-item">
		      <a class="nav-link text-dark" href="#contact">Contact</a>
		    </li>
		  </ul>
		</div>
    </nav>

	<!-- ------------------page content---------------------------------- -->
	<div class="container-fluid"> <!-- tab-content -->
    @yield('content')
	

	<!-- Footer -->
      <footer class="pt-4 my-md-5 pt-md-5 mx-5 border-top">
        <div class="row align-items-center">          
          <div class="col-6 col-md">
            <h5>Features</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="#">Cool stuff</a></li>
              <li><a class="text-muted" href="#">Random feature</a></li>
              <li><a class="text-muted" href="#">Team feature</a></li>
              <li><a class="text-muted" href="#">Stuff for developers</a></li>
              <li><a class="text-muted" href="#">Another one</a></li>
              <li><a class="text-muted" href="#">Last time</a></li>
            </ul>
          </div>
          <div class="col-6 col-md">
            <h5>Resources</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="#">Resource</a></li>
              <li><a class="text-muted" href="#">Resource name</a></li>
              <li><a class="text-muted" href="#">Another resource</a></li>
              <li><a class="text-muted" href="#">Final resource</a></li>
            </ul>
          </div>
          <div class="col-6 col-md">
            <h5>About</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="#">Team</a></li>
              <li><a class="text-muted" href="#">Locations</a></li>
              <li><a class="text-muted" href="#">Privacy</a></li>
              <li><a class="text-muted" href="#">Terms</a></li>
            </ul>
          </div>
          <div class="col-6 col-md">
            <h5>Social Media</h5>
              <ul class="list-unstyled text-small">
              	<li><i class="fa fa-facebook-official"></i></li>
          			<li><i class="fa fa-instagram"></i></li>
          			<li><i class="fa fa-snapchat"></i></li>
          			<li><i class="fa fa-pinterest-p"></i></li>
          			<li><i class="fa fa-twitter"></i></li>
          			<li><i class="fa fa-linkedin"></i></li>
              </ul>
          </div>
        </div>
      </footer>
    </div>
    <!-- ------------------page content end---------------------------------- -->

	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../../assets/js/vendor/popper.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="../../assets/js/vendor/holder.min.js"></script> -->
    <script>
      Holder.addTheme('thumb', {
        bg: '#55595c',
        fg: '#eceeef',
        text: 'Thumbnail'
      });
    </script>
  	
</body>
</html>